import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        ws: null,
        username: '',
        attendanceRecord: [],
        attendanceId: null
    },
    mutations: {
        //清除ws实例
        clearWs(state) {
            state.ws = null;
        },
        //重置考勤id
        resetAttendanceId(state) {
            state.attendanceId = null;
        },
        //重置考勤记录数组
        resetAttendanceRecord(state) {
            state.attendanceRecord = [];
        },
        //改变其中一条记录的考勤状态
        changeAttendanceRecord(state, params) {
            let index = 0;
            for (let i = 0; i < state.attendanceRecord.length; i++) {
                if (params.student_id === state.attendanceRecord[i].student_id) {
                    index = i;
                    break;
                }
            }
            Vue.set(state.attendanceRecord, index, params.attendance);
        },
        //向考勤记录数组中添加新的数据
        addNewRecord(state, newRecord) {
            let length = state.attendanceRecord.length;
            state.attendanceRecord.splice(length, 0, newRecord);
        }
    },
    actions: {},
    modules: {}
});
