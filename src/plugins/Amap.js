export let creatMap = function (dom) {
    return new AMap.Map(dom, {
        resizeEnable: true,//是否允许缩放地图
        zoom: 18//地图显示的缩放级别
    });
};
