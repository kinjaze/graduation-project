export var lineOption = {
    title: {
        text: '今日出勤数据'
    },
    tooltip: {
        trigger: 'axis'
    },
    legend: {
        data: ['出勤人数', '缺勤人数']
    },
    grid: {
        x: 35,
        y: 35,
        x2: 25,
        y2: 35

    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
        data: ['2021/5/1', '2021/5/1', '2021/5/1', '2021/5/1', '2021/5/1', '2021/5/1', '2021/5/1']
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            formatter: '{value}人'
        }
    },
    series: [
        {
            name: '出勤人数',
            type: 'line',
            smooth: true,
            data: [10, 11, 13, 11, 12, 12, 9],
            markPoint: {
                data: [
                    { type: 'max', name: '最大值' },
                    { type: 'min', name: '最小值' }
                ]
            }
        },
        {
            name: '缺勤人数',
            type: 'line',
            data: [1, -2, 2, 5, 3, 2, 0],
            smooth: true,
            markPoint: {
                data: [
                    { type: 'max', name: '最大值' },
                    { type: 'min', name: '最小值' }
                ]
            },
            lineStyle: {
                color: 'red'
            },
            itemStyle: {
                color: 'red'
            }
        }
    ]
};

