/*
*二次封装的axios函数,封装了get和post方法
* */
import axios from 'axios';
import qs from 'qs';

//切换开发环境时更换ip地址
if (process.env.NODE_ENV === 'development') {
    axios.defaults.baseURL = 'http://127.0.1:5000/';
} else if (process.env.NODE_ENV === 'production') {
    axios.defaults.baseURL = 'http://81.70.30.216:5000/';
}

//设置请求超时时间
axios.defaults.timeout = 3000;

// 默认返回一个axios实例
export default axios;

/*
* @desc 二次封装的axios请求方法
* @params 传入的参数  OBJ类型默认为null
* @methods 请求的方法 GET 或者 POST
* */
export function httpApi(url, params = null, methods) {
    if (methods === 'get') {
        return new Promise((resolve, reject) => {
            axios.get(url, {
                params: params
            }).then(res => {
                resolve(res);
            }).catch(err => {
                reject(err);
            });
        });
    } else if (methods === 'post') {
        return new Promise((resolve, reject) => {
            axios.post(url, qs.stringify(params))
                .then(res => {
                    resolve(res);
                })
                .catch(err => {
                    reject(err);
                });
        });
    } else {
        return axios;
    }
}




