/*
* 存放该项目下的所有的API
*
* */
import { httpApi } from '@/utils/axiosUtils';

export default {
    user: {
        //登录接口（POST）
        login: params => httpApi('api/pc/user/account/login', params, 'post'),
        //注册接口（POST）
        register: params => httpApi('api/pc/user/account/register', params, 'post'),
        //查询个人信息
        findByName: params => httpApi('api/pc/user/account/findByName', params, 'post'),
        //重置密码(POST)
        resetPassword: params => httpApi('api/pc/user/account/resetPassword', params, 'post'),
        //重置邮箱
        resetEmail: params => httpApi('api/pc/user/account/resetEmail', params, 'post'),
        //校验用户名是否重复(POST)
        check: params => httpApi('api/pc/user/account/check', params, 'post'),
        //校验教师编号是否重复
        checkId: params => httpApi('api/pc/user/account/checkId', params, 'post'),
        //教师个人信息认证
        info: params => httpApi('api/pc/user/teacherInfo/change', params, 'post')
    },
    email: {
        //发送邮箱验证码（POST）
        send: params => httpApi('api/pc/user/email/send', params, 'post'),
        //校验邮箱验证码是否正确（POST）
        check: params => httpApi('api/pc/user/email/check', params, 'post')
    },
    courses: {
        //获取所有的课程数据
        findAll: params => httpApi('api/pc/courses/courseInfo/findAll', params, 'post'),
        //添加课程
        add: params => httpApi('api/pc/courses/courseInfo/add', params, 'post'),
        //删除课程
        delete: params => httpApi('api/pc/courses/courseInfo/delete', params, 'post'),
        //更新课程
        update: params => httpApi('api/pc/courses/courseInfo/update', params, 'post')
    },
    attendance: {
        //添加新的考勤记录
        add: params => httpApi('api/pc/attendance/add', params, 'post'),
        //将未参加考勤的学生的的状态设置为缺勤
        setNotCheck: params => httpApi('api/pc/attendance/supplement', params, 'post'),
        //查看考勤记录
        findRecord: params => httpApi('api/pc/attendance/findRecordByTime', params, 'post'),
        //查看考勤记录通过考勤id
        findById: params => httpApi('api/pc/attendance/findRecordById', params, 'post'),
        //更新考勤状态
        updateRecord: params => httpApi('api/pc/attendance/changeRecord', params, 'post'),
        //获取该课程的学生总人数
        getTotalStudentNum: params => httpApi('api/pc/attendance/studentNum', params, 'post')
    },
    webSocketUrl: 'ws://localhost:5001'

};
